package tp.notee;

public class Courriel {
    private String emailDestination;
    private String titre;
    private String corps;
    //private File piecesJointes;
    private boolean piecesJointes;
    
    public Courriel() {
	this.setEmailDestination(null);
	this.setTitre(null);
	this.setCorps("");
	//this.setPiecesJointes(/home/e20170002768/Téléchargements/laposte.pdf);
	this.setPiecesJointes(false);
    }
    
    public Courriel(String emailDestination, String titre, String corps, boolean piecesJointes) {
    	this.setEmailDestination(emailDestination);
	    this.setTitre(titre);
        this.setCorps(corps);
        this.setPiecesJointes(piecesJointes);
    }

    public String getEmailDestination() {
	return this.emailDestination;
    }
    public void setEmailDestination(String email) {
	this.emailDestination = email;
    }
    
    public String getTitre() {
	return this.titre;
    }
    public void setTitre(String title) {
	this.titre = title;
    }

    public String getCorps() {
	return this.corps;
    }
    public void setCorps(String newCorps) {
	this.corps = newCorps;
    }

    public boolean isPiecesJointes() {
	return this.piecesJointes;
    }
    public void setPiecesJointes(boolean pj) {
	this.piecesJointes = pj;
    }

    public void envoyer() {
    	
    }

    public String toString() {
	return "[Email : "+this.getEmailDestination()+
	    ", \nTitre : "+this.getTitre()+
	    ", \nCorps : "+this.getCorps()+
	    ", \nPJ? : "+this.isPiecesJointes()+"]";
    }

}
    
