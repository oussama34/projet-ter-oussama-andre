package tp.notee;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.*;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.params.*;

import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

public class MainCourrielTest {

	Courriel c0, c1, c2, c3;
	
	@BeforeEach
	void setUp() {
		c0 = new Courriel();
		c1 = new Courriel("abcd@gmail.com","test n°1",
				"abfofhqjlcqobbvqubeobuovbebvo",false);
		c2 = new Courriel(null,"test n°2",
				"iubfcbieiebvbev PJ:/home/e20170002768/Téléchargements/laposte.pdf",true);
		c3 = new Courriel("fff@gmail.com","test n°2",
				"iubfcbieiebvbev PJ :/home/e20170002768/Téléchargements/laposte.pdf",true);
	}
	
	
	@Test
	public void testPresenceEmail() {
		if (c0.getEmailDestination() != null) {
			System.out.println("l'email est bien present dans c0");
		}
		else {
			System.out.println("l'email n'est pas present dans c0");
		}
		if (c1.getEmailDestination() != null) {
			System.out.println("l'email est bien present dans c1");
		}
		else {
			System.out.println("l'email n'est pas present dans c1");
		}
		if (c2.getEmailDestination() != null) {
			System.out.println("l'email est bien present dans c2");
		}
		else {
			System.out.println("l'email n'est pas present dans c2");
		}
		if (c3.getEmailDestination() != null) {
			System.out.println("l'email est bien present dans c3");
		}
		else {
			System.out.println("l'email n'est pas present dans c3");
		}
	}
	
	@Test
	void testPresenceTitre() {
		if (c0.getTitre() != null) {
			System.out.println("le titre est bien present dans c0");
		}
		else {
			System.out.println("le titre n'est pas present dans c0");
		}
		if (c1.getTitre() != null) {
			System.out.println("le titre est bien present dans c1");
		}
		else {
			System.out.println("le titre n'est pas present dans c1");
		}
		if (c2.getTitre() != null) {
			System.out.println("le titre est bien present dans c2");
		}
		else {
			System.out.println("le titre n'est pas present dans c2");
		}
		if (c3.getTitre() != null) {
			System.out.println("le titre est bien present dans c3");
		}
		else {
			System.out.println("le titre n'est pas present dans c3");
		}
	}
	
	@Test
	void testPresencePiecesJointes() {
		/* marche pas pour c0 quand on initialise corps a null 
		 * mais marche bien pour les autres 
		 */
		/*
		if((c0.getCorps().indexOf("PJ") != -1) 
				|| (c0.getCorps().indexOf("joint") != -1) 
				|| (c0.getCorps().indexOf("jointe") != -1)) {
			System.out.println("pieces jointes bien presentes dans c0");
		}
		else {
			System.out.println("pieces jointes non presentes dans c0");
		}
		if((c1.getCorps().indexOf("PJ") != -1) 
				|| (c1.getCorps().indexOf("joint") != -1) 
				|| (c1.getCorps().indexOf("jointe") != -1)) {
			System.out.println("pieces jointes bien presentes dans c1");
		}
		else {
			System.out.println("pieces jointes non presentes dans c1");
		}
		if((c2.getCorps().indexOf("PJ") != -1) 
				|| (c2.getCorps().indexOf("joint") != -1) 
				|| (c2.getCorps().indexOf("jointe") != -1)) {
			System.out.println("pieces jointes bien presentes dans c2");
		}
		else {
			System.out.println("pieces jointes non presentes dans c2");
		}
		if((c3.getCorps().indexOf("PJ") != -1) 
				|| (c3.getCorps().indexOf("joint") != -1) 
				|| (c3.getCorps().indexOf("jointe") != -1)) {
			System.out.println("pieces jointes bien presentes dans c3");
		}
		else {
			System.out.println("pieces jointes non presentes dans c3");
		}*/
		
		
		/* marche pas pour c0 quand on initialise corps a null 
		 * mais marche bien pour les autres 
		 */
		if((c0.getCorps().contains("PJ")) || (c0.getCorps().contains("joint")) 
				|| (c0.getCorps().contains("jointe"))) {
			System.out.println("pieces jointes bien presentes dans c0");
		}
		else {
			System.out.println("pieces jointes non presentes dans c0");
		}
		if((c1.getCorps().contains("PJ")) || (c1.getCorps().contains("joint")) 
				|| (c1.getCorps().contains("jointe"))) {
			System.out.println("pieces jointes bien presentes dans c1");
		}
		else {
			System.out.println("pieces jointes non presentes dans c1");
		}
		if((c2.getCorps().contains("PJ")) || (c2.getCorps().contains("joint")) 
				|| (c2.getCorps().contains("jointe"))) {
			System.out.println("pieces jointes bien presentes dans c2");
		}
		else {
			System.out.println("pieces jointes non presentes dans c2");
		}
		if((c3.getCorps().contains("PJ")) || (c3.getCorps().contains("joint")) 
				|| (c3.getCorps().contains("jointe"))) {
			System.out.println("pieces jointes bien presentes dans c3");
		}
		else {
			System.out.println("pieces jointes non presentes dans c3");
		}
		
	}
	
	@Test 
	void testEmailBienFormerc1() {
		if(c1.getEmailDestination().matches("(.*)@(.*)")) {
			System.out.println("email c1 a motier bien former");
		}
		else {
			System.out.println("email c1 mal former");
		}
	}
	
	@Test 
	void testEmailBienFormer() {
		//rajouter pour le cas ou c'est null
		/*if(c0.getEmailDestination().matches("(.*)@(.*)")) {
			System.out.println("email c0 a motier bien former");
		}
		else {
			System.out.println("email c0 mal former");
		}*/
		if(c1.getEmailDestination().matches("(.*)@(.*)")) {
			System.out.println("email c1 a motier bien former");
		}
		else {
			System.out.println("email c1 mal former");
		}
		/*if(c2.getEmailDestination().matches("(.*)@(.*)")) {
			System.out.println("email c2 a motier bien former");
		}
		else {
			System.out.println("email c2 mal former");
		}*/
		if(c3.getEmailDestination().matches("(.*)@(.*)")) {
			System.out.println("email c3 a motier bien former");
		}
		else {
			System.out.println("email c3 mal former");
		}
	}
	
	/*
	class testonsLesTestsParams {
		@ParameterizedTest
	    //@NullSource
	    //@EmptySource
	    @MethodSource("testEmailBienFormerc1")
	    //@ValueSource(strings = { "", "  ", "\t", "\n"})
	    void emailBienFormerc1test(String text) {
			//assertTrue(text == null || text.trim().isEmpty());
	        assertNotNull(text);
	    }
	}
	*/
}
